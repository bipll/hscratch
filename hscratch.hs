-- hscratch by bipll, 2010. See LICENSE for lawish stuff.

import System
import Data.WAVE
import Data.Int
import Data.Bits
import Array
import Debug.Trace

main = do
	(diskname:scratchname:strData) <- getArgs
	WAVE (WAVEHeader nChans wRate _ (Just nFrames)) disk <- getWAVEFile diskname
	let
		points = map (strToPt wRate) strData
		((s0,t0,_):(s1,_,_):_) = points
		newHeader = WAVEHeader nChans wRate 24 Nothing
			in putWAVEFile scratchname $ WAVE newHeader $ scratch (crop nFrames disk) nFrames points

strToPt :: Int -> String -> (Double,Double,Int)
strToPt _ "" = (0.0,0.0,0)
strToPt r s = let
		(st,tl) = break (== ':') s
		(tm,tl') = if null tl then ("1.0","") else break (== ':') $ tail tl
		mt = if null tl' then 0 else case tl' of
						(_:'c':_) -> 0
						(_:'p':_) -> 1
						(_:'s':_) -> 2
						(_:'d':_) -> 3
						otherwise -> read $ tail tl'
		sampleRead s = case s of
					('s':ss) -> read ss
					ss -> read ss * (fromIntegral r)
			in (sampleRead st,sampleRead tm,mt)

crop :: Int -> [[Int32]] -> Array Int [Double]
crop nFrames = array (0,nFrames - 1) . zip [0 .. nFrames - 1] . map (map (fromIntegral . flip shiftR 8))

scratch :: Array Int [Double] -> Int -> [(Double,Double,Int)] -> [[Int32]]
scratch disk diskLen ((s0,t0,mt):p1@(s1,_,_):ps) = map (placeStylus disk diskLen) ((methods !! mt) s0 s1 t0 diskLen) ++ (scratch disk diskLen (p1:ps))
scratch disk diskLen [(s0,_,_)] = [placeStylus disk diskLen s0]
scratch _ _ _ = []

placeStylus :: Array Int [Double] -> Int -> Double -> [Int32]
placeStylus disk diskLen position = let
					(pos',c) = properFraction position
					pos = pos' `mod` diskLen
					nxtPos = if pos == diskLen - 1 then 0 else pos + 1
						in map (flip shiftL 8 . round) $ zipWith (\x y -> (1.0 - c) * x + c * y) (disk!pos) (disk!nxtPos)

methods :: [Double -> Double -> Double -> Int -> [Double]]
methods = [cosine,parabolic,speedLimit,delayedSpeedLimit]

waveGuide :: (Double -> Double -> Double -> Double -> Double) -> Double -> Double -> Double -> Int -> [Double]
waveGuide f start fin time diskLen = let
					(samps,fracSamps) = properFraction time
					nSamp = if fracSamps > 0 then samps else samps - 1
					doubleDiskLen = fromInteger $ fromIntegral diskLen
						in map ((f start fin time) . fromInteger) [0 .. nSamp]

cosine = waveGuide (\s f dl x -> (s - f) / 2.0 * cos(pi * (x / dl)) + (s + f) / 2)
parabolic = waveGuide (\s f dl x -> let {a = 2.0 * (f - s) / (dl * dl); ksi = dl - x} in if x <= dl / 2.0 then s + a * x * x else f - a * ksi * ksi)
speedLimit = waveGuide speedLimitCurve
delayedSpeedLimit = waveGuide (\s f dl x -> if x < 0.05 * dl then s else if x > 0.95 * dl then f else speedLimitCurve s f (0.9 * dl) (x - 0.05 * dl))

speedLimitCurve :: Double -> Double -> Double -> Double -> Double
speedLimitCurve s f dl x = let {d = dl / 3.0; m = (f - s) / 2.0; den = d * d + d; a = m / den; b = s + m - m * 3.0 * d / den; ksi = dl - x} in if x < d then s + a * x * x else if x < 2.0 * d then a * x + b else f - a * ksi * ksi
